program Example_15;

uses
  Vcl.Forms,
  uFrmMain in 'uFrmMain.pas' {Form1},
  Vcl.Themes,
  Vcl.Styles,
  uDM in 'uDM.pas' {DM: TDataModule};

{$R *.res}

begin
   Application.Initialize;
   Application.MainFormOnTaskbar := True;

   {$IFDEF DEBUG}
      Application.Title := 'Example 15 (Debug)';
      TStyleManager.TrySetStyle('Carbon');
   {$ELSE}
      Application.Title := 'Example 15 (Release)';
      TStyleManager.TrySetStyle('Luna');
   {$ENDIF}

   Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TDM, DM);
  Application.Run;

end.
