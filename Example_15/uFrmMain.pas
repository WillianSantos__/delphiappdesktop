unit uFrmMain;

interface

uses
   Winapi.Windows,
   Winapi.Messages,
   System.SysUtils,
   System.Variants,
   System.Classes,
   Vcl.Graphics,
   Vcl.Controls,
   Vcl.Forms,
   Vcl.Dialogs,
   Vcl.StdCtrls,
   frxClass,
   frxDBSet,
   frxExportPDF;

type
   TForm1 = class(TForm)
      Button1: TButton;
      Memo1: TMemo;
      frxReportMain: TfrxReport;
      frxPDFExport1: TfrxPDFExport;
      frxDBDatasetCustomers: TfrxDBDataset;
      procedure FormCreate(Sender: TObject);
      procedure Button1Click(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form1: TForm1;

implementation

uses
   uDM;

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
   Self.Caption := Application.Title;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
   frxReportMain.ShowReport;
end;

end.
