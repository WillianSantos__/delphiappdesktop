unit uDM;

interface

uses
   System.SysUtils,
   System.Classes,
   // Phys
   FireDAC.Stan.ExprFuncs,
   FireDAC.Phys.SQLiteDef,
   FireDAC.Stan.Intf,
   FireDAC.Phys,
   FireDAC.Phys.SQLite,
   // Wait
   FireDAC.UI.Intf,
   FireDAC.VCLUI.Wait,
   FireDAC.Comp.UI,
   // Conn
   Data.DB,
   FireDAC.Stan.Option,
   FireDAC.Stan.Error,
   FireDAC.Phys.Intf,
   FireDAC.Stan.Def,
   FireDAC.Stan.Pool,
   FireDAC.Stan.Async,
   FireDAC.Comp.Client, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet;

type
   TDM = class(TDataModule)
    FDPhysSQLiteDriverLink: TFDPhysSQLiteDriverLink;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    FDConn: TFDConnection;
    FDQryCustomers: TFDQuery;
    FDTableNames: TFDTable;
    procedure FDConnBeforeConnect(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   DM: TDM;

implementation

{$R *.dfm}

procedure TDM.FDConnBeforeConnect(Sender: TObject);
var
   LFileDB: string;
begin
   FDConn.LoginPrompt := False;
   FDConn.DriverName := 'SQLite';

   LFileDB := ExtractFilePath(ParamStr(0)) + 'Example_15_Employees.s3db';

   FDConn.Params.Values['LockingMode'] := 'lmNormal';
   FDConn.Params.Values['Database'] := LFileDB;
end;

end.
