object DM: TDM
  OldCreateOrder = False
  Height = 339
  Width = 405
  object FDPhysSQLiteDriverLink: TFDPhysSQLiteDriverLink
    Left = 64
    Top = 16
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 64
    Top = 64
  end
  object FDConn: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\Edesoft07\Documents\Embarcadero\Studio\Project' +
        's\Git\_Development\Binary\Example_15_Employees.s3db'
      'LockingMode=Normal'
      'DriverID=SQLite')
    LoginPrompt = False
    BeforeConnect = FDConnBeforeConnect
    Left = 64
    Top = 112
  end
  object FDQryCustomers: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM CUSTOMERS ORDER BY FIRSTNAME, LASTNAME')
    Left = 64
    Top = 160
  end
  object FDTableNames: TFDTable
    Connection = FDConn
    Left = 184
    Top = 16
  end
end
