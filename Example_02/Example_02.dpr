program Example_02;

uses
  Vcl.Forms,
  uFrmMain in 'uFrmMain.pas' {Form1},
  Vcl.Themes,
  Vcl.Styles,
  uClassEmployee in 'Class\uClassEmployee.pas',
  uClassPessoa in 'Class\uClassPessoa.pas';

{$R *.res}

begin
   Application.Initialize;
   Application.MainFormOnTaskbar := True;

   {$IFDEF DEBUG}
      Application.Title := 'Example 02 (Debug)';
      TStyleManager.TrySetStyle('Carbon');
   {$ELSE}
      Application.Title := 'Example 02 (Release)';
      TStyleManager.TrySetStyle('Luna');
   {$ENDIF}

   Application.CreateForm(TForm1, Form1);
  Application.Run;

end.
