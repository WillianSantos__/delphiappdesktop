unit uFrmMain;

interface

uses
   Winapi.Windows,
   Winapi.Messages,
   System.SysUtils,
   System.Variants,
   System.Classes,
   Vcl.Graphics,
   Vcl.Controls,
   Vcl.Forms,
   Vcl.Dialogs,
   Vcl.StdCtrls;

type
   TForm1 = class(TForm)
      Button1: TButton;
      Memo1: TMemo;
      Button2: TButton;
      procedure FormCreate(Sender: TObject);
      procedure Button1Click(Sender: TObject);
      procedure Button2Click(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form1: TForm1;

implementation

uses
   uClassEmployee,
   uClassPessoa;

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
   Self.Caption := Application.Title;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
   Employee: TEmployee;
begin
   Employee := TEmployee.Create;
   try
      Employee.Name := 'Will';
      Employee.Title := 'Developer';
      Employee.HourlyPayRate := 16;

      Memo1.Lines.Clear;
      Memo1.Lines.Add('Calculate Pay: ' + Employee.CalculatePay.ToString);
   finally
      // Employee.Free;
      // Employee := nil;
      FreeAndNil(Employee);
   end;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
   Pessoa: TPessoa;
begin
   Pessoa := TPessoa.Create;

   Pessoa.Nome := 'Willian';
   Pessoa.Sobrenome := 'Santos';

   Memo1.Lines.Clear;
   Memo1.Lines.Add('Nome Completo: ' + Pessoa.Nome + ' ' + Pessoa.Sobrenome);

   Pessoa.Free;
end;

end.
