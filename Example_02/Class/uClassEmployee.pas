unit uClassEmployee;

interface

type
   TEmployee = class { (TObject) }

   private
      FName: string;
      FAbone: integer;
      FTitle: string;
      FHourlyPayRate: Double;

   public
      constructor Create;

      property Name: string read FName write FName;
      property Title: string read FTitle write FTitle;
      property Abone: integer read FAbone { write FAbone };
      property HourlyPayRate: Double read FHourlyPayRate write FHourlyPayRate;

      function CalculatePay: Double;
   end;

implementation

{ TEmployee }

function TEmployee.CalculatePay: Double;
begin
   Result := FHourlyPayRate * FAbone;
end;

constructor TEmployee.Create;
begin
   FAbone := 8;
end;

end.
