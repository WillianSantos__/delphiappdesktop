object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 211
  ClientWidth = 418
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 412
    Height = 25
    Align = alTop
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Memo1: TMemo
    AlignWithMargins = True
    Left = 3
    Top = 65
    Width = 412
    Height = 143
    Align = alClient
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
    ExplicitTop = 34
    ExplicitHeight = 174
  end
  object Button2: TButton
    AlignWithMargins = True
    Left = 3
    Top = 34
    Width = 412
    Height = 25
    Align = alTop
    Caption = 'Button2'
    TabOrder = 2
    OnClick = Button2Click
    ExplicitLeft = 6
    ExplicitTop = 11
  end
end
