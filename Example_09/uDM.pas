unit uDM;

interface

uses
   System.SysUtils,
   System.Classes,
   FireDAC.Stan.ExprFuncs,
   FireDAC.Phys.SQLiteDef,
   FireDAC.Stan.Intf,
   FireDAC.Phys,
   FireDAC.Phys.SQLite,
   FireDAC.Stan.Option,
   FireDAC.Stan.Error,
   FireDAC.UI.Intf,
   FireDAC.Phys.Intf,
   FireDAC.Stan.Def,
   FireDAC.Stan.Pool,
   FireDAC.Stan.Async,
   FireDAC.VCLUI.Wait,
   Data.DB,
   FireDAC.Comp.Client,
   FireDAC.Stan.Param,
   FireDAC.DatS,
   FireDAC.DApt.Intf,
   FireDAC.DApt,
   FireDAC.Comp.DataSet,
   FireDAC.Comp.UI,
   FireDAC.Stan.StorageJSON,
   FireDAC.Stan.StorageXML,
   FireDAC.Stan.StorageBin;

type
   TDM = class(TDataModule)
      FDPhysSQLiteDriverLink: TFDPhysSQLiteDriverLink;
      FDConn: TFDConnection;
      FDQryOpen: TFDQuery;
      FDQryExec: TFDQuery;
      FDGUIxWaitCursor: TFDGUIxWaitCursor;
    FDQryOpenID: TIntegerField;
    FDQryOpenName: TWideStringField;
    FDQryOpenDepartment: TWideStringField;
    FDQryOpenSeniority: TIntegerField;
      procedure FDConnBeforeConnect(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   DM: TDM;

implementation

{$R *.dfm}

procedure TDM.FDConnBeforeConnect(Sender: TObject);
var
   LFileDB: string;
begin
   FDConn.LoginPrompt := False;
   FDConn.DriverName := 'SQLite';

   {$IFDEF DEBUG}
   LFileDB := ExtractFilePath(ParamStr(0)) + 'Example_09_Employees.s3db';
   FDConn.Params.Values['LockingMode'] := 'lmNormal';
   {$ELSE}
   LFileDB := ExtractFilePath(ParamStr(0)) + 'Example_09_Employees.s3db';
   FDConn.Params.Values['LockingMode'] := 'lmExclusive';
   {$ENDIF}
   FDConn.Params.Values['Database'] := LFileDB;
end;

end.
