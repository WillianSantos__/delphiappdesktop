object DM: TDM
  OldCreateOrder = False
  Height = 338
  Width = 492
  object FDPhysSQLiteDriverLink: TFDPhysSQLiteDriverLink
    Left = 80
    Top = 16
  end
  object FDConn: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\wos\Documents\Embarcadero\Studio\Projects\Delp' +
        'hiAppDesktop\_Development\Binary\Example_09_Employees.s3db'
      'LockingMode=Normal'
      'DriverID=SQLite')
    LoginPrompt = False
    BeforeConnect = FDConnBeforeConnect
    Left = 80
    Top = 112
  end
  object FDQryOpen: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT ID, '
      'cast(Name as varchar) as Name,'
      'cast(Department as varchar) as Department,'
      'Seniority'
      'FROM EMPLOYEE')
    Left = 80
    Top = 160
    object FDQryOpenID: TIntegerField
      DisplayLabel = 'Id'
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object FDQryOpenName: TWideStringField
      AutoGenerateValue = arDefault
      DisplayWidth = 30
      FieldName = 'Name'
      Origin = 'Name'
      ProviderFlags = []
      ReadOnly = True
      Size = 32767
    end
    object FDQryOpenDepartment: TWideStringField
      AutoGenerateValue = arDefault
      DisplayWidth = 30
      FieldName = 'Department'
      Origin = 'Department'
      ProviderFlags = []
      ReadOnly = True
      Size = 32767
    end
    object FDQryOpenSeniority: TIntegerField
      FieldName = 'Seniority'
      Origin = 'Seniority'
    end
  end
  object FDQryExec: TFDQuery
    Connection = FDConn
    Left = 80
    Top = 209
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 80
    Top = 64
  end
end
