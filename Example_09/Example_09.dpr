program Example_09;

uses
   Vcl.Forms,
   Vcl.Themes,
   Vcl.Styles,
   uFrmMain in 'uFrmMain.pas' {FrmMain} ,
   uDM in 'uDM.pas' {DM: TDataModule};

{$R *.res}

begin
   Application.Initialize;
   Application.MainFormOnTaskbar := True;
   Application.Title := 'Example 09';
   TStyleManager.TrySetStyle('Luna');
   Application.CreateForm(TDM, DM);
   Application.CreateForm(TFrmMain, FrmMain);

   Application.Run;

end.
