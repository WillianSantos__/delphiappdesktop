object FrmMain: TFrmMain
  Left = 0
  Top = 0
  Caption = 'FrmMain'
  ClientHeight = 297
  ClientWidth = 714
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblField: TLabel
    Left = 8
    Top = 8
    Width = 22
    Height = 13
    Caption = 'Field'
  end
  object lblValue: TLabel
    Left = 152
    Top = 8
    Width = 26
    Height = 13
    Caption = 'Value'
  end
  object dbGrd: TDBGrid
    Left = 8
    Top = 64
    Width = 698
    Height = 225
    DataSource = dsDataSource
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object dbNav: TDBNavigator
    Left = 391
    Top = 21
    Width = 315
    Height = 25
    DataSource = dsDataSource
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
    TabOrder = 1
  end
  object edtField: TEdit
    Left = 8
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'Name'
  end
  object edtValue: TEdit
    Left = 152
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object btnGo: TButton
    Left = 296
    Top = 22
    Width = 75
    Height = 25
    Caption = 'Go'
    TabOrder = 4
    OnClick = btnGoClick
  end
  object dsDataSource: TDataSource
    DataSet = DM.FDQryOpen
    Left = 488
    Top = 72
  end
end
