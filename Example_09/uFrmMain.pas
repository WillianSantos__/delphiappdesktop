unit uFrmMain;

interface

uses
   Winapi.Windows,
   Winapi.Messages,
   System.SysUtils,
   System.Variants,
   System.Classes,
   Vcl.Graphics,
   Vcl.Controls,
   Vcl.Forms,
   Vcl.Dialogs,
   System.UITypes,
   Vcl.StdCtrls,
   FireDAC.Stan.Intf,
   FireDAC.Stan.Option,
   FireDAC.Stan.Param,
   FireDAC.Stan.Error,
   FireDAC.DatS,
   FireDAC.Phys.Intf,
   FireDAC.DApt.Intf,
   FireDAC.Stan.StorageBin,
   Data.DB,
   FireDAC.Comp.DataSet,
   FireDAC.Comp.Client,
   Vcl.Grids,
   Vcl.DBGrids,
   Vcl.Mask,
   Vcl.DBCtrls,
   Vcl.ExtCtrls;

type
   TFrmMain = class(TForm)
      dbGrd: TDBGrid;
      dsDataSource: TDataSource;
      dbNav: TDBNavigator;
      lblField: TLabel;
      edtField: TEdit;
      lblValue: TLabel;
      edtValue: TEdit;
      btnGo: TButton;
      procedure FormCreate(Sender: TObject);
      procedure FormKeyPress(Sender: TObject; var Key: Char);
      procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
      procedure FormShow(Sender: TObject);
      procedure btnGoClick(Sender: TObject);
   private
      procedure RefreshData;
      { Private declarations }
   public
      { Public declarations }
   end;

var
   FrmMain: TFrmMain;

implementation

uses
   uDM;

{$R *.dfm}

procedure TFrmMain.RefreshData;
begin
   dsDataSource.DataSet.Close;
   dsDataSource.DataSet.Open;
end;

procedure TFrmMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   {$REGION 'without API of Windows'}
   // if MessageDlg('Exit now?', mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
   // CanClose := True
   // else
   // CanClose := False; { todo: Cancel the closing form }
   {$ENDREGION}
   {$REGION 'with API of Windows'}
   if MessageBox(Handle, 'Exit now?', PChar(Application.Title) //
      , MB_YESNO + MB_SYSTEMMODAL + MB_ICONQUESTION + MB_DEFBUTTON1) = ID_YES then
      CanClose := True
   else
      CanClose := False; { todo: Cancel the closing form }
   {$ENDREGION}
end;

procedure TFrmMain.FormCreate(Sender: TObject);
begin
   // Self.Caption := Application.Title;
   Self.Caption := 'Form Main';
   Self.KeyPreview := True;
   Self.BorderIcons := [biSystemMenu, biMaximize];
   Self.BorderStyle := TFormBorderStyle.bsSingle;
   // Self.FormStyle := TFormStyle.fsMDIChild;
   Self.Position := TPosition.poScreenCenter;
   Self.Height := 340;
   Self.Width := 730;
end;

procedure TFrmMain.FormShow(Sender: TObject);
begin
   RefreshData;
end;

procedure TFrmMain.FormKeyPress(Sender: TObject; var Key: Char);
begin
   { todo: Esc for Close Form }
   if (Key = #27) then
   begin
      Key := #0;
      Self.Close;
   end;

   { todo: Enter to Tab }
   if (Key = #13) then
   begin
      Key := #0;
      Perform(WM_NEXTDLGCTL, 0, 0);
   end;
end;

procedure TFrmMain.btnGoClick(Sender: TObject);
begin
   dsDataSource.DataSet.Filtered := False;
   // dsDataSource.DataSet.Filter := edtField.Text + ' = ' + QuotedStr(edtValue.Text);
   // dsDataSource.DataSet.Filter := edtField.Text + ' like ' + QuotedStr('%' + edtValue.Text + '%');

   dsDataSource.DataSet.Filter := edtField.Text + ' = ' + edtValue.Text;
   // dsDataSource.DataSet.Filter := edtField.Text + ' >= ' + edtValue.Text;
   // dsDataSource.DataSet.Filter := edtField.Text + ' <= ' + edtValue.Text;

   dsDataSource.DataSet.Filtered := True;
end;

end.
