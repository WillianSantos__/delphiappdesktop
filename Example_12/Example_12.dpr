program Example_12;

uses
   Vcl.Forms,
   uFrmMain in 'uFrmMain.pas' {Form1} ,
   Vcl.Themes,
   Vcl.Styles;

{$R *.res}

begin
   Application.Initialize;
   Application.MainFormOnTaskbar := True;

   {$IFDEF DEBUG}
      Application.Title := 'Example 12 (Debug)';
      TStyleManager.TrySetStyle('Carbon');
   {$ELSE}
      Application.Title := 'Example 12 (Release)';
      TStyleManager.TrySetStyle('Luna');
   {$ENDIF}

   Application.CreateForm(TForm1, Form1);
   Application.Run;

end.
