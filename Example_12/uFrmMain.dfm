object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 458
  ClientWidth = 769
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 763
    Height = 25
    Align = alTop
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Memo1: TMemo
    AlignWithMargins = True
    Left = 3
    Top = 34
    Width = 763
    Height = 79
    Align = alTop
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
  end
  object PageControl1: TPageControl
    AlignWithMargins = True
    Left = 3
    Top = 119
    Width = 763
    Height = 336
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object Chart1: TChart
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 749
        Height = 302
        BackWall.Pen.Visible = False
        BottomWall.Brush.Gradient.EndColor = clSilver
        BottomWall.Brush.Gradient.StartColor = clGray
        BottomWall.Brush.Gradient.Visible = True
        BottomWall.Pen.Color = clGray
        BottomWall.Size = 4
        Gradient.Direction = gdFromTopLeft
        Gradient.EndColor = clWhite
        Gradient.StartColor = clSilver
        Gradient.Visible = True
        LeftWall.Brush.Gradient.EndColor = clSilver
        LeftWall.Brush.Gradient.StartColor = clGray
        LeftWall.Brush.Gradient.Visible = True
        LeftWall.Color = clWhite
        LeftWall.Pen.Color = clGray
        LeftWall.Size = 4
        MarginBottom = 5
        MarginLeft = 5
        MarginRight = 5
        MarginTop = 5
        Title.Text.Strings = (
          'TChart')
        BottomAxis.Grid.Color = 14540253
        BottomAxis.LabelsFormat.Font.Color = clGray
        BottomAxis.LabelsFormat.Font.Height = -9
        BottomAxis.LabelStyle = talValue
        Frame.Visible = False
        LeftAxis.Grid.Color = 14540253
        LeftAxis.LabelsFormat.Font.Color = clGray
        LeftAxis.LabelsFormat.Font.Height = -9
        LeftAxis.LabelStyle = talValue
        Zoom.Animated = True
        Align = alClient
        BevelWidth = 2
        Color = clWhite
        TabOrder = 0
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object Series1: TBarSeries
          Legend.Text = 'Example 12'
          LegendTitle = 'Example 12'
          BarBrush.Color = clNone
          BarBrush.Style = bsCross
          BarBrush.BackColor = clDefault
          BarBrush.Image.Data = {
            07544269746D61707E000000424D7E000000000000003E000000280000001000
            0000100000000100010000000000400000000000000000000000020000000200
            000000000000FFFFFF00AAAA0000FFFF0000AAAA0000FFFF0000AAAA0000FFFF
            0000AAAA0000FFFF0000AAAA0000FFFF0000AAAA0000FFFF0000AAAA0000FFFF
            0000AAAA0000FFFF0000}
          Marks.Frame.Visible = False
          MultiBar = mbStacked
          TickLines.Color = clDefault
          TickLines.Fill.Gradient.Visible = True
          TickLines.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Bar'
          YValues.Order = loNone
        end
        object Series2: TBarSeries
          MultiBar = mbStacked
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Bar'
          YValues.Order = loNone
        end
        object Series3: TBarSeries
          MultiBar = mbStacked
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Bar'
          YValues.Order = loNone
        end
      end
    end
  end
end
