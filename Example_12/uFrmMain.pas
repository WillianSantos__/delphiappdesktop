unit uFrmMain;

interface

uses
   Winapi.Windows,
   Winapi.Messages,
   System.SysUtils,
   System.Variants,
   System.Classes,
   Vcl.Graphics,
   Vcl.Controls,
   Vcl.Forms,
   Vcl.Dialogs,
   Vcl.StdCtrls,
   VclTee.TeeGDIPlus,
   VclTee.TeEngine,
   Vcl.ExtCtrls,
   VclTee.TeeProcs,
   VclTee.Chart,
   Vcl.ComCtrls,
   VclTee.Series;

type
   TForm1 = class(TForm)
      Button1: TButton;
      Memo1: TMemo;
      Chart1: TChart;
      PageControl1: TPageControl;
      TabSheet1: TTabSheet;
      Series1: TBarSeries;
      Series2: TBarSeries;
      Series3: TBarSeries;
      procedure FormCreate(Sender: TObject);
      procedure Button1Click(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
   Series1.Add(99.98, 'Delphi for Win32');
   Series1.Add(71.88, 'Delphi for Android');
   Series1.Add(49.54, 'Delphi for iOS');
   Series1.Add(57.97, 'Delphi for Linux');
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
   Self.Caption := Application.Title;
end;

end.
