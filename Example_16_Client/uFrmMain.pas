unit uFrmMain;

interface

uses
   Winapi.Windows,
   Winapi.Messages,
   System.SysUtils,
   System.Variants,
   System.Classes,
   Vcl.Graphics,
   Vcl.Controls,
   Vcl.Forms,
   Vcl.Dialogs,
   Vcl.StdCtrls;

type
   TForm1 = class(TForm)
      Button1: TButton;
      Memo1: TMemo;
      procedure FormCreate(Sender: TObject);
      procedure Button1Click(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form1: TForm1;

implementation

uses
   ClientModuleUnit1;

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
   Self.Caption := Application.Title;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
   strEchoString: string;
   strReverseString: string;
   strSum: string;

   doubleSumA: Double;
   doubleSumB: Double;
begin
   strEchoString := //
      ClientModule1.ServerMethods1Client.EchoString('Embarcadero | DataSnap');

   strReverseString := //
      ClientModule1.ServerMethods1Client.ReverseString(strEchoString);

   Randomize;
   doubleSumA := Random(999);
   doubleSumB := Random(999);

   strSum := //
      ClientModule1.ServerMethods1Client.Sum(doubleSumA, doubleSumB).ToString;

   Memo1.Lines.clear;
   Memo1.Lines.Add('Receive Datasnap');

   Memo1.Lines.Add('EchoString: ' + strEchoString);

   Memo1.Lines.Add('ReverseString: ' + strReverseString);

   Memo1.Lines.Add('Sum (A + B): ' + //
      doubleSumA.ToString + ' + ' + doubleSumB.ToString + ' = ' + strSum);
end;

end.
