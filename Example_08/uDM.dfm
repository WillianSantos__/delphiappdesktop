object DM: TDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 338
  Width = 492
  object FDPhysSQLiteDriverLink: TFDPhysSQLiteDriverLink
    Left = 80
    Top = 16
  end
  object FDConn: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\wos\Documents\Embarcadero\Studio\Projects\Delp' +
        'hiAppDesktop\_Development\Binary\SQLiteDebug.s3db'
      'LockingMode=Normal'
      'DriverID=SQLite')
    LoginPrompt = False
    AfterConnect = FDConnAfterConnect
    BeforeConnect = FDConnBeforeConnect
    Left = 80
    Top = 112
  end
  object FDQryOpen: TFDQuery
    Connection = FDConn
    Left = 80
    Top = 160
  end
  object FDQryExec: TFDQuery
    Connection = FDConn
    Left = 80
    Top = 208
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 80
    Top = 64
  end
  object FDQryUser: TFDQuery
    AfterPost = FDQryUserAfterPost
    Connection = FDConn
    SQL.Strings = (
      'SELECT '
      ' u.ID_USER'
      ',u.DS_USER'
      ',u.ID_COURSE'
      ',c.DS_COURSE'
      ',u.DT_REG_INS'
      'FROM TB_USER u'
      'INNER JOIN TB_COURSE c ON (c.ID_COURSE = u.ID_COURSE)')
    Left = 216
    Top = 16
    object FDQryUserID_USER: TFDAutoIncField
      DisplayLabel = 'Id'
      FieldName = 'ID_USER'
      Origin = 'ID_USER'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
      EditFormat = '0000'
    end
    object FDQryUserDS_USER: TStringField
      DisplayLabel = 'Name'
      DisplayWidth = 40
      FieldName = 'DS_USER'
      Origin = 'DS_USER'
      Size = 255
    end
    object FDQryUserID_COURSE: TIntegerField
      FieldName = 'ID_COURSE'
      Origin = 'ID_COURSE'
      Visible = False
    end
    object FDQryUserDS_COURSE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Couse'
      DisplayWidth = 20
      FieldName = 'DS_COURSE'
      Origin = 'DS_COURSE'
      ProviderFlags = []
      ReadOnly = True
      Size = 255
    end
    object FDQryUserDT_REG_INS: TDateTimeField
      DisplayLabel = 'Create'
      FieldName = 'DT_REG_INS'
      Origin = 'DT_REG_INS'
    end
  end
  object FDQryLkpCourse: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT ID_COURSE, DS_COURSE FROM TB_COURSE')
    Left = 216
    Top = 64
  end
end
