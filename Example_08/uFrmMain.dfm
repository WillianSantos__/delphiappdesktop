object FrmMain: TFrmMain
  Left = 0
  Top = 0
  Caption = 'FrmMain'
  ClientHeight = 297
  ClientWidth = 714
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblDS_USER: TLabel
    Left = 8
    Top = 8
    Width = 27
    Height = 13
    Caption = 'Name'
  end
  object lblID_COURSE: TLabel
    Left = 225
    Top = 8
    Width = 34
    Height = 13
    Caption = 'Course'
  end
  object dbGrd: TDBGrid
    Left = 8
    Top = 64
    Width = 698
    Height = 225
    DataSource = dsUser
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object edtDS_USER: TDBEdit
    Left = 8
    Top = 24
    Width = 200
    Height = 21
    DataField = 'DS_USER'
    DataSource = dsUser
    TabOrder = 1
  end
  object cbbID_COURSE: TDBLookupComboBox
    Left = 224
    Top = 24
    Width = 145
    Height = 21
    DataField = 'ID_COURSE'
    DataSource = dsUser
    KeyField = 'ID_COURSE'
    ListField = 'DS_COURSE'
    ListSource = dsLkpCourse
    TabOrder = 2
  end
  object dbNav: TDBNavigator
    Left = 391
    Top = 21
    Width = 315
    Height = 25
    DataSource = dsUser
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
    TabOrder = 3
  end
  object dsUser: TDataSource
    DataSet = DM.FDQryUser
    Left = 488
    Top = 72
  end
  object dsLkpCourse: TDataSource
    DataSet = DM.FDQryLkpCourse
    Left = 568
    Top = 72
  end
end
