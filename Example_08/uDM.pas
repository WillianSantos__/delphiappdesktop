unit uDM;

interface

uses
   System.SysUtils,
   System.Classes,
   FireDAC.Stan.ExprFuncs,
   FireDAC.Phys.SQLiteDef,
   FireDAC.Stan.Intf,
   FireDAC.Phys,
   FireDAC.Phys.SQLite,
   FireDAC.Stan.Option,
   FireDAC.Stan.Error,
   FireDAC.UI.Intf,
   FireDAC.Phys.Intf,
   FireDAC.Stan.Def,
   FireDAC.Stan.Pool,
   FireDAC.Stan.Async,
   FireDAC.VCLUI.Wait,
   Data.DB,
   FireDAC.Comp.Client,
   FireDAC.Stan.Param,
   FireDAC.DatS,
   FireDAC.DApt.Intf,
   FireDAC.DApt,
   FireDAC.Comp.DataSet,
   FireDAC.Comp.UI,
   FireDAC.Stan.StorageJSON,
   FireDAC.Stan.StorageXML,
   FireDAC.Stan.StorageBin;

type
   TDM = class(TDataModule)
      FDPhysSQLiteDriverLink: TFDPhysSQLiteDriverLink;
      FDConn: TFDConnection;
      FDQryOpen: TFDQuery;
      FDQryExec: TFDQuery;
      FDGUIxWaitCursor: TFDGUIxWaitCursor;
      FDQryUser: TFDQuery;
      FDQryLkpCourse: TFDQuery;
      FDQryUserID_USER: TFDAutoIncField;
      FDQryUserDS_USER: TStringField;
      FDQryUserID_COURSE: TIntegerField;
      FDQryUserDS_COURSE: TStringField;
      FDQryUserDT_REG_INS: TDateTimeField;
      procedure FDConnBeforeConnect(Sender: TObject);
      procedure DataModuleCreate(Sender: TObject);
      procedure FDConnAfterConnect(Sender: TObject);
      procedure FDQryUserAfterPost(DataSet: TDataSet);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   DM: TDM;

implementation

{$R *.dfm}

procedure TDM.FDConnBeforeConnect(Sender: TObject);
var
   LFileDB: string;
begin
   FDConn.LoginPrompt := False;
   FDConn.DriverName := 'SQLite';

   {$IFDEF DEBUG}
   LFileDB := ExtractFilePath(ParamStr(0)) + 'Example_08_SQLiteDebug.s3db';
   FDConn.Params.Values['LockingMode'] := 'lmNormal';
   {$ELSE}
   LFileDB := ExtractFilePath(ParamStr(0)) + 'Example_08_SQLiteRelease.s3db';
   FDConn.Params.Values['LockingMode'] := 'lmExclusive';
   {$ENDIF}
   FDConn.Params.Values['Database'] := LFileDB;
end;

procedure TDM.FDConnAfterConnect(Sender: TObject);
var
   strSQL_TABLE_CREATE: string;
begin
   strSQL_TABLE_CREATE := //
      'DROP TABLE IF EXISTS TB_COURSE; CREATE TABLE IF NOT EXISTS TB_COURSE (ID_COURSE INTEGER PRIMARY KEY AUTOINCREMENT, DS_COURSE VARCHAR(255), DT_REG_INS DATETIME DEFAULT CURRENT_TIMESTAMP);' + //
      'INSERT INTO TB_COURSE (DS_COURSE) VALUES (' + QuotedStr('Delphi') + ');' + //
      'INSERT INTO TB_COURSE (DS_COURSE) VALUES (' + QuotedStr('Android') + ');' + //
      'INSERT INTO TB_COURSE (DS_COURSE) VALUES (' + QuotedStr('iOS') + ');' + //
      'INSERT INTO TB_COURSE (DS_COURSE) VALUES (' + QuotedStr('Linux') + ');' + //
      'INSERT INTO TB_COURSE (DS_COURSE) VALUES (' + QuotedStr('IoT') + ');' + //
      'CREATE TABLE IF NOT EXISTS TB_USER (ID_USER INTEGER PRIMARY KEY AUTOINCREMENT, ID_COURSE INTEGER, DS_USER VARCHAR(255), DT_REG_INS DATETIME DEFAULT CURRENT_TIMESTAMP);';

   FDConn.ExecSQL(strSQL_TABLE_CREATE);
end;

procedure TDM.DataModuleCreate(Sender: TObject);
begin
   FDConn.Close;
   FDConn.Open;
end;

procedure TDM.FDQryUserAfterPost(DataSet: TDataSet);
begin
   FDQryUser.Close;
   FDQryUser.Open;
end;

end.
