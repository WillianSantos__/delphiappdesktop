
{****************************************************************************************************************}
{                                                                                                                }
{                                                XML Data Binding                                                }
{                                                                                                                }
{         Generated on: 03/03/2018 15:00:31                                                                      }
{       Generated from: C:\Users\Edesoft07\Documents\Embarcadero\Studio\Projects\Git\Example_14\Example_14.xml   }
{   Settings stored in: C:\Users\Edesoft07\Documents\Embarcadero\Studio\Projects\Git\Example_14\Example_14.xdb   }
{                                                                                                                }
{****************************************************************************************************************}

unit Example_14_Interface;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLProdutosType = interface;
  IXMLProdutoType = interface;

{ IXMLProdutosType }

  IXMLProdutosType = interface(IXMLNodeCollection)
    ['{D289434B-61CB-4D9A-80DF-06D8FD31B204}']
    { Property Accessors }
    function Get_Produto(Index: Integer): IXMLProdutoType;
    { Methods & Properties }
    function Add: IXMLProdutoType;
    function Insert(const Index: Integer): IXMLProdutoType;
    property Produto[Index: Integer]: IXMLProdutoType read Get_Produto; default;
  end;

{ IXMLProdutoType }

  IXMLProdutoType = interface(IXMLNode)
    ['{BF2030D3-64DF-44E1-A5EE-4C000B8CEF09}']
    { Property Accessors }
    function Get_Codigo: Integer;
    function Get_Nome: UnicodeString;
    function Get_Quantidade: Integer;
    function Get_Preco: Double;

    procedure Set_Codigo(Value: Integer);
    procedure Set_Nome(Value: UnicodeString);
    procedure Set_Quantidade(Value: Integer);
    procedure Set_Preco(Value: Double);
    { Methods & Properties }
    property Codigo: Integer read Get_Codigo write Set_Codigo;
    property Nome: UnicodeString read Get_Nome write Set_Nome;
    property Quantidade: Integer read Get_Quantidade write Set_Quantidade;
    property Preco: Double read Get_Preco write Set_Preco;
  end;

{ Forward Decls }

  TXMLProdutosType = class;
  TXMLProdutoType = class;

{ TXMLProdutosType }

  TXMLProdutosType = class(TXMLNodeCollection, IXMLProdutosType)
  protected
    { IXMLProdutosType }
    function Get_Produto(Index: Integer): IXMLProdutoType;
    function Add: IXMLProdutoType;
    function Insert(const Index: Integer): IXMLProdutoType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLProdutoType }

  TXMLProdutoType = class(TXMLNode, IXMLProdutoType)
  protected
    { IXMLProdutoType }
    function Get_Codigo: Integer;
    function Get_Nome: UnicodeString;
    function Get_Quantidade: Integer;
    function Get_Preco: Double;

    procedure Set_Codigo(Value: Integer);
    procedure Set_Nome(Value: UnicodeString);
    procedure Set_Quantidade(Value: Integer);
    procedure Set_Preco(Value: Double);
  end;

{ Global Functions }

function GetProdutos(Doc: IXMLDocument): IXMLProdutosType;
function LoadProdutos(const FileName: string): IXMLProdutosType;
function NewProdutos: IXMLProdutosType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetProdutos(Doc: IXMLDocument): IXMLProdutosType;
begin
  Result := Doc.GetDocBinding('Produtos', TXMLProdutosType, TargetNamespace) as IXMLProdutosType;
end;

function LoadProdutos(const FileName: string): IXMLProdutosType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Produtos', TXMLProdutosType, TargetNamespace) as IXMLProdutosType;
end;

function NewProdutos: IXMLProdutosType;
begin
  Result := NewXMLDocument.GetDocBinding('Produtos', TXMLProdutosType, TargetNamespace) as IXMLProdutosType;
end;

{ TXMLProdutosType }

procedure TXMLProdutosType.AfterConstruction;
begin
  RegisterChildNode('Produto', TXMLProdutoType);
  ItemTag := 'Produto';
  ItemInterface := IXMLProdutoType;
  inherited;
end;

function TXMLProdutosType.Get_Produto(Index: Integer): IXMLProdutoType;
begin
  Result := List[Index] as IXMLProdutoType;
end;

function TXMLProdutosType.Add: IXMLProdutoType;
begin
  Result := AddItem(-1) as IXMLProdutoType;
end;

function TXMLProdutosType.Insert(const Index: Integer): IXMLProdutoType;
begin
  Result := AddItem(Index) as IXMLProdutoType;
end;

{ TXMLProdutoType }

function TXMLProdutoType.Get_Codigo: Integer;
begin
  Result := AttributeNodes['Codigo'].NodeValue;
end;

procedure TXMLProdutoType.Set_Codigo(Value: Integer);
begin
  SetAttribute('Codigo', Value);
end;

function TXMLProdutoType.Get_Nome: UnicodeString;
begin
  Result := ChildNodes['Nome'].Text;
end;

procedure TXMLProdutoType.Set_Nome(Value: UnicodeString);
begin
  ChildNodes['Nome'].NodeValue := Value;
end;

function TXMLProdutoType.Get_Quantidade: Integer;
begin
  Result := ChildNodes['Quantidade'].NodeValue;
end;

procedure TXMLProdutoType.Set_Quantidade(Value: Integer);
begin
  ChildNodes['Quantidade'].NodeValue := Value;
end;

function TXMLProdutoType.Get_Preco: Double;
begin
  Result := ChildNodes['Preco'].NodeValue;
end;

procedure TXMLProdutoType.Set_Preco(Value: Double);
begin
  ChildNodes['Preco'].NodeValue := Value;
end;

end.