unit uFrmMain;

interface

uses
   Winapi.Windows,
   Winapi.Messages,
   System.SysUtils,
   System.StrUtils,
   System.Variants,
   System.Classes,
   Vcl.Graphics,
   Vcl.Controls,
   Vcl.Forms,
   Vcl.Dialogs,
   Vcl.StdCtrls,
   Vcl.ExtCtrls,
   Vcl.ComCtrls,
   Xml.xmldom,
   Xml.XMLIntf,
   Xml.XMLDoc,
   Example_14_Interface;

type
   TForm1 = class(TForm)
      TreeView1: TTreeView;
      XMLDocument1: TXMLDocument;
      lblCodigo: TLabel;
      edtCodigo: TEdit;
      btnInserir: TButton;
      lblNome: TLabel;
      edtNome: TEdit;
      btnRemover: TButton;
      lblQuantidade: TLabel;
      edtQuantidade: TEdit;
      btnSalvar: TButton;
      lblPreco: TLabel;
      edtPreco: TEdit;
      btnAtualizar: TButton;

      procedure FormCreate(Sender: TObject);
      procedure TreeView1Click(Sender: TObject);
      procedure btnAtualizarClick(Sender: TObject);
      procedure btnInserirClick(Sender: TObject);
      procedure btnRemoverClick(Sender: TObject);
      procedure btnSalvarClick(Sender: TObject);
   private
      Produto: IXMLProdutoType;
      Estoque: IXMLProdutosType;

      procedure AdicionaNo(NoXml: IXMLNode; Pai: TTreeNode);
      procedure AtualizaTreeView;
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
   Self.Caption := Application.Title;
   Estoque := GetProdutos(XMLDocument1);
   AtualizaTreeView;
end;

procedure TForm1.AdicionaNo(NoXml: IXMLNode; Pai: TTreeNode);
var
   SubNoXml: IXMLNode;
   Filho: TTreeNode;
   i: Integer;
begin
   Filho := TreeView1.Items.AddChild(Pai, NoXml.NodeName);

   for i := 0 to Pred(NoXml.AttributeNodes.Count) do
   begin
      Filho.Text := Filho.Text + ' ' + //
         NoXml.AttributeNodes.Get(i).NodeName + ' = ' + //
         QuotedStr(NoXml.AttributeNodes.Get(i).Text);
   end;

   if NoXml.HasChildNodes then
   begin
      for i := 0 to Pred(NoXml.ChildNodes.Count) do
      begin
         case NoXml.ChildNodes[i].NodeType of
            ntText:
               begin
                  TreeView1.Items.AddChild(Filho, NoXml.Text);
               end;
         else
            SubNoXml := NoXml.ChildNodes[i];
            AdicionaNo(SubNoXml, Filho);
         end;
      end;
   end;
end;

procedure TForm1.AtualizaTreeView;
begin
   TreeView1.Items.Clear;
   AdicionaNo(Estoque, nil);
   TreeView1.Items.GetFirstNode.Expand(False);
end;

procedure TForm1.TreeView1Click(Sender: TObject);
var
   NoAtivo: TTreeNode;
begin
   if AnsiContainsText(TreeView1.Selected.Text, 'Codigo') then
   begin
      NoAtivo := TreeView1.Selected;
      Produto := Estoque.Produto[NoAtivo.Parent.IndexOf(NoAtivo)];

      edtCodigo.Text := Produto.Codigo.ToString;
      edtNome.Text := Produto.Nome;
      edtQuantidade.Text := Produto.Quantidade.ToString;
      edtPreco.Text := Format('%3.2f', [Produto.Preco]);
   end
   else
   begin
      Produto := nil;
      edtCodigo.Text := EmptyStr;
      edtNome.Text := EmptyStr;
      edtQuantidade.Text := EmptyStr;
      edtPreco.Text := EmptyStr;
   end;
end;

procedure TForm1.btnInserirClick(Sender: TObject);
begin
   Produto := Estoque.Add;

   Produto.Codigo := 0;
   Produto.Nome := '<Nome>';
   Produto.Quantidade := 0;
   Produto.Preco := 0;

   AtualizaTreeView;
end;

procedure TForm1.btnRemoverClick(Sender: TObject);
var
   NoAtivo: TTreeNode;
   i: Integer;
begin
   NoAtivo := TreeView1.Selected;
   i := NoAtivo.Parent.IndexOf(NoAtivo);
   Estoque.Delete(i);

   AtualizaTreeView;
end;

procedure TForm1.btnSalvarClick(Sender: TObject);
begin
   XMLDocument1.SaveToFile;
end;

procedure TForm1.btnAtualizarClick(Sender: TObject);
begin
   Produto.Codigo := StrToInt(edtCodigo.Text);
   Produto.Nome := edtNome.Text;
   Produto.Quantidade := StrToInt(edtQuantidade.Text);
   Produto.Preco := StrToFloat(edtPreco.Text);

   AtualizaTreeView;
end;

end.
