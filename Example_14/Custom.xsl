<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<body>
				<xsl:for-each select="Document/Customer">
					<br/>
					<font face="Arial" size="3">
						<b>
							<!--CUSTOMER-->
							<xsl:value-of select="COMPANY"/>,
							<!--ID-->
							<xsl:value-of select="CUSTNO"/>:
						</b>
					</font>
					<br/>
					<!-- Table of Customer's Orders -->
					<table width="100%" border="1" cellspacing="0" cellpadding="2" bordercolor="#000000">
						<!--HEADER ROW -->
						<tr bgcolor="#FFFFCC" bordercolor="#000000">
							<td width="25%">
								<div align="center">
									<b>
										<font face="Arial" size="2">Order #</font>
									</b>
								</div>
							</td>
							<td width="25%">
								<div align="center">
									<b>
										<font face="Arial" size="2">Shipment Method</font>
									</b>
								</div>
							</td>
							<td width="25%">
								<div align="center">
									<b>
										<font face="Arial" size="2">Order Total</font>
									</b>
								</div>
							</td>
							<td width="25%">
								<div align="center">
									<b>
										<font face="Arial" size="2">Amount Paid</font>
									</b>
								</div>
							</td>
						</tr>
						<xsl:for-each select="Orders/OrderRecord">
							<tr>
								<xsl:choose>
									<xsl:when test="position() mod 2 = 0">
										<xsl:attribute name="bgcolor">#F4F4F4</xsl:attribute>
										<xsl:attribute name="bordercolor">#000000</xsl:attribute>
									</xsl:when>
									<xsl:otherwise>
										<xsl:attribute name="bordercolor">#000000</xsl:attribute>
									</xsl:otherwise>
								</xsl:choose>
								<td width="25%">
									<div align="center">
										<font face="Arial" size="2">
											<a>
												<xsl:value-of select="ORDERNO"/>
											</a>
										</font>
									</div>
								</td>
								<td width="25%">
									<div align="center">
										<font face="Arial" size="2">
											<xsl:value-of select="SHIPVIA"/>
										</font>
									</div>
								</td>
								<td width="25%">
									<div align="center">
										<font face="Arial" size="2">
											<xsl:value-of select="ITEMSTOTAL"/>
										</font>
									</div>
								</td>
								<xsl:choose>
									<xsl:when test="AMOUNTPAID &lt; ITEMSTOTAL">
										<td width="25%" bgcolor="RED">
											<div align="center">
												<font face="Arial" size="2">$
													<xsl:value-of select="AMOUNTPAID"/>
												</font>
											</div>
										</td>
									</xsl:when>
									<xsl:otherwise>
										<td width="25%">
											<div align="center">
												<font face="Arial" size="2">$
													<xsl:value-of select="AMOUNTPAID"/>
												</font>
											</div>
										</td>
									</xsl:otherwise>
								</xsl:choose>
							</tr>
						</xsl:for-each>
					</table>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>