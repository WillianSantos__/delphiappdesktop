object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 450
  ClientWidth = 558
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblCodigo: TLabel
    Left = 16
    Top = 376
    Width = 43
    Height = 13
    Caption = 'lblCodigo'
  end
  object lblNome: TLabel
    Left = 152
    Top = 376
    Width = 37
    Height = 13
    Caption = 'lblNome'
  end
  object lblQuantidade: TLabel
    Left = 287
    Top = 376
    Width = 66
    Height = 13
    Caption = 'lblQuantidade'
  end
  object lblPreco: TLabel
    Left = 424
    Top = 376
    Width = 37
    Height = 13
    Caption = 'lblPreco'
  end
  object TreeView1: TTreeView
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 552
    Height = 358
    Align = alTop
    Indent = 19
    TabOrder = 0
    OnClick = TreeView1Click
  end
  object edtCodigo: TEdit
    Left = 16
    Top = 392
    Width = 121
    Height = 21
    TabOrder = 1
    Text = 'edtCodigo'
  end
  object btnInserir: TButton
    Left = 16
    Top = 417
    Width = 121
    Height = 25
    Caption = 'btnInserir'
    TabOrder = 2
    OnClick = btnInserirClick
  end
  object edtNome: TEdit
    Left = 152
    Top = 392
    Width = 121
    Height = 21
    TabOrder = 3
    Text = 'edtNome'
  end
  object btnRemover: TButton
    Left = 152
    Top = 417
    Width = 121
    Height = 25
    Caption = 'btnRemover'
    TabOrder = 4
    OnClick = btnRemoverClick
  end
  object edtQuantidade: TEdit
    Left = 287
    Top = 392
    Width = 121
    Height = 21
    TabOrder = 5
    Text = 'edtQuantidade'
  end
  object btnSalvar: TButton
    Left = 287
    Top = 417
    Width = 121
    Height = 25
    Caption = 'btnSalvar'
    TabOrder = 6
    OnClick = btnSalvarClick
  end
  object edtPreco: TEdit
    Left = 424
    Top = 392
    Width = 121
    Height = 21
    TabOrder = 7
    Text = 'edtPreco'
  end
  object btnAtualizar: TButton
    Left = 424
    Top = 416
    Width = 121
    Height = 25
    Caption = 'btnAtualizar'
    TabOrder = 8
    OnClick = btnAtualizarClick
  end
  object XMLDocument1: TXMLDocument
    FileName = 
      'C:\Users\Edesoft07\Documents\Embarcadero\Studio\Projects\Git\Exa' +
      'mple_14\Example_14.xml'
    Left = 56
    Top = 24
  end
end
