unit uDM;

interface

uses
   System.SysUtils,
   System.Classes,
   FireDAC.Stan.ExprFuncs,
   FireDAC.Phys.SQLiteDef,
   FireDAC.Stan.Intf,
   FireDAC.Phys,
   FireDAC.Phys.SQLite,
   FireDAC.Stan.Option,
   FireDAC.Stan.Error,
   FireDAC.UI.Intf,
   FireDAC.Phys.Intf,
   FireDAC.Stan.Def,
   FireDAC.Stan.Pool,
   FireDAC.Stan.Async,
   FireDAC.VCLUI.Wait,
   Data.DB,
   FireDAC.Comp.Client,
   FireDAC.Stan.Param,
   FireDAC.DatS,
   FireDAC.DApt.Intf,
   FireDAC.DApt,
   FireDAC.Comp.DataSet,
   FireDAC.Comp.UI, FireDAC.Stan.StorageJSON, FireDAC.Stan.StorageXML,
  FireDAC.Stan.StorageBin;

type
   TDM = class(TDataModule)
      FDPhysSQLiteDriverLink: TFDPhysSQLiteDriverLink;
      FDConn: TFDConnection;
      FDQryOpen: TFDQuery;
      FDQryExec: TFDQuery;
      FDGUIxWaitCursor: TFDGUIxWaitCursor;
      FDQryUser: TFDQuery;
      procedure FDConnBeforeConnect(Sender: TObject);
      procedure DataModuleCreate(Sender: TObject);
      procedure FDConnAfterConnect(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   DM: TDM;

implementation

{$R *.dfm}

procedure TDM.FDConnBeforeConnect(Sender: TObject);
var
   LFileDB: string;
begin
   FDConn.LoginPrompt := False;
   FDConn.DriverName := 'SQLite';

   {$IFDEF DEBUG}
   LFileDB := ExtractFilePath(ParamStr(0)) + 'SQLiteDebug.s3db';
   FDConn.Params.Values['LockingMode'] := 'lmNormal';
   {$ELSE}
   LFileDB := ExtractFilePath(ParamStr(0)) + 'SQLiteRelease.s3db';
   FDConn.Params.Values['LockingMode'] := 'lmExclusive';
   {$ENDIF}
   FDConn.Params.Values['Database'] := LFileDB;
end;

procedure TDM.FDConnAfterConnect(Sender: TObject);
const
   SQL_TABLE_CREATE = //
      'CREATE TABLE IF NOT EXISTS TB_COURSE (ID_COURSE INTEGER PRIMARY KEY AUTOINCREMENT, DS_COURSE VARCHAR(255), DT_REG_INS DATETIME DEFAULT CURRENT_TIMESTAMP);' + //
      'CREATE TABLE IF NOT EXISTS TB_USER (ID_USER INTEGER PRIMARY KEY AUTOINCREMENT, DS_USER VARCHAR(255), DT_REG_INS DATETIME DEFAULT CURRENT_TIMESTAMP);';
begin
   FDConn.ExecSQL(SQL_TABLE_CREATE);
end;

procedure TDM.DataModuleCreate(Sender: TObject);
begin
   FDConn.Close;
   FDConn.Open;
end;

end.
