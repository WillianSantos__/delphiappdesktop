program Example_05;

uses
  Vcl.Forms,
  uDM in 'uDM.pas' {DM: TDataModule},
  uFrmModel in 'MVC\uFrmModel.pas' {FrmModel},
  uFrmView in 'MVC\uFrmView.pas' {FrmView},
  uFrmController in 'MVC\uFrmController.pas' {FrmController},
  uFrmUser in 'uFrmUser.pas' {FrmUser},
  Vcl.Themes,
  Vcl.Styles;

{$R *.res}

begin
   Application.Initialize;
   Application.MainFormOnTaskbar := True;
   Application.Title := 'Example 05';
  TStyleManager.TrySetStyle('Luna');
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TFrmUser, FrmUser);
  Application.Run;

end.
