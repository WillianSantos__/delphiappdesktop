unit uFrmUser;

interface

uses
   Winapi.Windows,
   Winapi.Messages,
   System.SysUtils,
   System.Variants,
   System.Classes,
   Vcl.Graphics,
   Vcl.Controls,
   Vcl.Forms,
   Vcl.Dialogs,
   Vcl.StdCtrls,
   Vcl.Buttons,
   Vcl.ExtCtrls,
   Vcl.Grids,
   Vcl.DBGrids,
   Vcl.ComCtrls,
   FireDAC.Stan.ExprFuncs,
   FireDAC.Phys.SQLiteDef,
   FireDAC.Stan.Intf,
   FireDAC.Phys,
   FireDAC.Phys.SQLite,
   FireDAC.Stan.Option,
   FireDAC.Stan.Error,
   FireDAC.UI.Intf,
   FireDAC.Phys.Intf,
   FireDAC.Stan.Def,
   FireDAC.Stan.Pool,
   FireDAC.Stan.Async,
   FireDAC.VCLUI.Wait,
   Data.DB,
   FireDAC.Comp.Client,
   FireDAC.Stan.Param,
   FireDAC.DatS,
   FireDAC.DApt.Intf,
   FireDAC.DApt,
   FireDAC.Comp.DataSet,
   FireDAC.Comp.UI,

   uFrmView,
   Vcl.DBCtrls,
   Vcl.Mask;

type
   TFrmUser = class(TFrmView)
      lblID_USER: TDBText;
      lblID_USER_: TLabel;
      lblDS_USER: TLabel;
      edtDS_USER: TDBEdit;
      lblDT_REG_INS_: TLabel;
      lblDT_REG_INS: TDBText;
      procedure FormCreate(Sender: TObject);
      procedure btnNewClick(Sender: TObject);
      procedure btnEditClick(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   FrmUser: TFrmUser;

implementation

uses
   uDM;

{$R *.dfm}

procedure TFrmUser.FormCreate(Sender: TObject);
begin
   inherited;
   Self.Caption := 'Form User';
   Self.Height := 400;
   dsDataSource.DataSet := GetList('TB_USER');
end;

procedure TFrmUser.btnNewClick(Sender: TObject);
begin
   inherited;
   edtDS_USER.SetFocus;
   edtDS_USER.SelStart := 0;
end;

procedure TFrmUser.btnEditClick(Sender: TObject);
begin
   inherited;
   edtDS_USER.SetFocus;
   edtDS_USER.SelStart := Length(edtDS_USER.Text);
end;

end.
