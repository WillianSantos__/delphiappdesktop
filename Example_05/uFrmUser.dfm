inherited FrmUser: TFrmUser
  Caption = 'FrmUser'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pgcUI: TPageControl
    inherited tsList: TTabSheet
      inherited grdList: TDBGrid
        Columns = <
          item
            Expanded = False
            FieldName = 'ID_USER'
            Title.Alignment = taCenter
            Title.Caption = 'Id'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = [fsBold]
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DS_USER'
            Title.Alignment = taCenter
            Title.Caption = 'Users'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = [fsBold]
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_REG_INS'
            Title.Alignment = taCenter
            Title.Caption = 'Create'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = [fsBold]
            Width = 150
            Visible = True
          end>
      end
      inherited pnlList: TPanel
        inherited DBNavigator1: TDBNavigator
          Hints.Strings = ()
        end
      end
    end
    inherited tsData: TTabSheet
      object lblID_USER: TDBText [0]
        Left = 8
        Top = 27
        Width = 53
        Height = 13
        AutoSize = True
        DataField = 'ID_USER'
        DataSource = dsDataSource
      end
      object lblID_USER_: TLabel [1]
        Left = 8
        Top = 8
        Width = 12
        Height = 13
        Caption = 'Id'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblDS_USER: TLabel [2]
        Left = 8
        Top = 50
        Width = 26
        Height = 13
        Caption = 'User'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblDT_REG_INS_: TLabel [3]
        Left = 8
        Top = 104
        Width = 89
        Height = 13
        Caption = 'Create Register'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblDT_REG_INS: TDBText [4]
        Left = 8
        Top = 123
        Width = 72
        Height = 13
        AutoSize = True
        DataField = 'DT_REG_INS'
        DataSource = dsDataSource
      end
      object edtDS_USER: TDBEdit
        Left = 8
        Top = 69
        Width = 200
        Height = 21
        DataField = 'DS_USER'
        DataSource = dsDataSource
        TabOrder = 1
      end
    end
  end
end
