unit uFrmView;

interface

uses
   Winapi.Windows,
   Winapi.Messages,
   System.SysUtils,
   System.Variants,
   System.Classes,
   Vcl.Graphics,
   Vcl.Controls,
   Vcl.Forms,
   Vcl.Dialogs,
   Data.DB,
   Vcl.ExtCtrls,
   Vcl.ComCtrls,
   Vcl.Grids,
   Vcl.DBGrids,
   Vcl.StdCtrls,
   Vcl.Buttons,

   uFrmController, Vcl.DBCtrls;

type
   TFrmView = class(TFrmController)
      pgcUI: TPageControl;
      tsList: TTabSheet;
      tsData: TTabSheet;
      grdList: TDBGrid;
      pnlList: TPanel;
      pnlData: TPanel;
      btnClose: TBitBtn;
      btnPrint: TBitBtn;
      btnDelete: TBitBtn;
      btnEdit: TBitBtn;
      btnNew: TBitBtn;
      btnCancel: TBitBtn;
      btnPost: TBitBtn;
    DBNavigator1: TDBNavigator;
      procedure FormCreate(Sender: TObject);
      procedure btnNewClick(Sender: TObject);
      procedure btnEditClick(Sender: TObject);
      procedure btnDeleteClick(Sender: TObject);
      procedure btnPrintClick(Sender: TObject);
      procedure btnCloseClick(Sender: TObject);
      procedure btnPostClick(Sender: TObject);
      procedure btnCancelClick(Sender: TObject);
      procedure dsDataSourceStateChange(Sender: TObject);
      procedure grdListDblClick(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   FrmView: TFrmView;

implementation

uses
   uDM;

{$R *.dfm}

procedure TFrmView.FormCreate(Sender: TObject);
begin
   inherited;
   Self.Caption := 'Form User Interface';
   pgcUI.ActivePage := tsList;
   tsList.TabVisible := True;
   tsData.TabVisible := False;

   { todo: To not delete records with CTRL + DEL or CTRL + Shift + DEL }
   grdList.ReadOnly := True;

   { todo: DBGrid for Lists, configuration default }
   grdList.Options := [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, //
      dgRowSelect, dgTitleClick, dgTitleHotTrack];

end;

procedure TFrmView.btnNewClick(Sender: TObject);
begin
   inherited;
   // dsDataSource.DataSet.Insert; // Adiciona um registro na tabela aonde estiver
   dsDataSource.DataSet.Append; // Adiciona um registro no final da tabela
   pgcUI.ActivePage := tsData;
   tsData.TabVisible := True;
   tsList.TabVisible := False;
end;

procedure TFrmView.btnEditClick(Sender: TObject);
begin
   inherited;
   dsDataSource.DataSet.Edit;
   pgcUI.ActivePage := tsData;
   tsData.TabVisible := True;
   tsList.TabVisible := False;
end;

procedure TFrmView.grdListDblClick(Sender: TObject);
begin
   inherited;
   // btnEditClick(Sender);
   btnEdit.Click;
end;

procedure TFrmView.btnDeleteClick(Sender: TObject);
begin
   inherited;
   // if MessageDlg('Deseja realmente excluir o registro?' //
   // , mtConfirmation, [mbYes, mbNo], 0) = mrYes then
   // begin
   //
   // end;

   if MessageBox(Handle //
      , 'Deseja realmente excluir o registro?' //
      , PWideChar(Application.Title) //
      , MB_YESNO + MB_ICONQUESTION) = IDYES then
   begin
      dsDataSource.DataSet.Delete;
   end;
end;

procedure TFrmView.btnPostClick(Sender: TObject);
begin
   inherited;
   if dsDataSource.DataSet.State in [TDataSetState.dsInsert, TDataSetState.dsEdit] then
   begin
      dsDataSource.DataSet.Post;
      dsDataSource.DataSet.Close;
      dsDataSource.DataSet.Open;
      pgcUI.ActivePage := tsList;
      tsList.TabVisible := True;
      tsData.TabVisible := False;
   end;
end;

procedure TFrmView.btnCancelClick(Sender: TObject);
begin
   inherited;
   if dsDataSource.DataSet.State in [TDataSetState.dsInsert, TDataSetState.dsEdit] then
   begin
      dsDataSource.DataSet.cancel;
      pgcUI.ActivePage := tsList;
      tsList.TabVisible := True;
      tsData.TabVisible := False;
   end;
end;

procedure TFrmView.btnPrintClick(Sender: TObject);
begin
   inherited;
   // ListPrint
end;

procedure TFrmView.dsDataSourceStateChange(Sender: TObject);
begin
   inherited;
   btnNew.Enabled := dsDataSource.DataSet.State in [TDataSetState.dsBrowse];
   btnEdit.Enabled := dsDataSource.DataSet.State in [TDataSetState.dsBrowse];
   btnDelete.Enabled := (dsDataSource.DataSet.State in [TDataSetState.dsBrowse]) //
      and (dsDataSource.DataSet.RecordCount > 0);
   btnPrint.Enabled := dsDataSource.DataSet.State in [TDataSetState.dsBrowse];
   btnPost.Enabled := dsDataSource.DataSet.State in [TDataSetState.dsInsert, TDataSetState.dsEdit];
   btnCancel.Enabled := dsDataSource.DataSet.State in [TDataSetState.dsInsert, TDataSetState.dsEdit];
end;

procedure TFrmView.btnCloseClick(Sender: TObject);
begin
   inherited;
   Self.Close;
end;

end.
