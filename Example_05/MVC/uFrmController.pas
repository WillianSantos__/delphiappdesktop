unit uFrmController;

interface

uses
   Winapi.Windows,
   Winapi.Messages,
   System.SysUtils,
   System.Variants,
   System.Classes,
   Vcl.Graphics,
   Vcl.Controls,
   Vcl.Forms,
   Vcl.Dialogs,
   Vcl.StdCtrls,
   Data.DB,
   FireDAC.Comp.Client,

   uFrmModel;

type
   TFrmController = class(TFrmModel)
      dsDataSource: TDataSource;
      procedure FormCreate(Sender: TObject);

   private
      { Private declarations }
   public
      function GetList(ATable: string): TFDQuery;
      function GetData(ATable, AField: string; AId: integer): TFDQuery;
      { Public declarations }
   end;

var
   FrmController: TFrmController;

implementation

uses
   uDM;

{$R *.dfm}

function TFrmController.GetList(ATable: string): TFDQuery;
var
   LFDQuery: TFDQuery;
   LSQL: string;
const
   cSQL = 'SELECT * FROM %s ORDER BY 1';
begin
   LSQL := Format(cSQL, [ATable]);

   LFDQuery := TFDQuery.Create(Self);
   try
      LFDQuery.Connection := DM.FDConn;
      LFDQuery.Name := 'LFDQuery' + FormatDateTime('yyyymmdd_hhnnsszzz_', Now) + VarToStr(Random(9999));
      LFDQuery.Open(LSQL);

      Result := LFDQuery;
   except
      on E: Exception do
      begin
         Result := nil;

         MessageBox(Handle //
            , PChar(E.ClassName + sLineBreak + E.Message) //
            , PChar(Application.Title) //
            , MB_OK + MB_SYSTEMMODAL + MB_ICONERROR);
      end;
   end;
end;

function TFrmController.GetData(ATable, AField: string; AId: integer): TFDQuery;
var
   LFDQuery: TFDQuery;
   LSQL: string;
begin
   LSQL := 'SELECT * FROM ' + ATable + ' WHERE ' + AField + ' = ' + AId.ToString;

   LFDQuery := TFDQuery.Create(Self);
   try
      DM.FDQryOpen.Open(LSQL);
      Result := DM.FDQryOpen;
   except
      on E: Exception do
      begin
         Result := nil;

         MessageBox(Handle //
            , PChar(E.ClassName + sLineBreak + E.Message) //
            , PChar(Application.Title) //
            , MB_OK + MB_SYSTEMMODAL + MB_ICONERROR);
      end;
   end;
end;

procedure TFrmController.FormCreate(Sender: TObject);
begin
   inherited;
   Self.Caption := 'Form Data';
end;

end.
