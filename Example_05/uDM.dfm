object DM: TDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 338
  Width = 492
  object FDPhysSQLiteDriverLink: TFDPhysSQLiteDriverLink
    Left = 80
    Top = 16
  end
  object FDConn: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\wos\Documents\Embarcadero\Studio\Projects\Delp' +
        'hiAppDesktop\_Development\Binary\SQLiteDebug.s3db'
      'DriverID=SQLite')
    LoginPrompt = False
    AfterConnect = FDConnAfterConnect
    BeforeConnect = FDConnBeforeConnect
    Left = 80
    Top = 112
  end
  object FDQryOpen: TFDQuery
    Connection = FDConn
    Left = 80
    Top = 160
  end
  object FDQryExec: TFDQuery
    Connection = FDConn
    Left = 80
    Top = 208
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 80
    Top = 64
  end
  object FDQryUser: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM TB_USER')
    Left = 216
    Top = 16
  end
end
