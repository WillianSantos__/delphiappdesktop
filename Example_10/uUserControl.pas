unit uUserControl;

interface

type
   TUserControl = class
   private
      FName: String;
      FUsername: String;

      procedure SetName(Value: String);
      procedure SetUsername(Value: String);

      constructor Create;
   public
      function DoUserLogin: Boolean;
      function ValidateUser(Username, Password: String): Boolean;

      property Name: String read FName write SetName;
      property Username: String read FUsername write SetUsername;

      class function GetInstance: TUserControl;

   strict private
      class var FInstance: TUserControl;
   end;

implementation

uses
   uLogin,
   uUser,
   System.UITypes;

constructor TUserControl.Create;
begin
   inherited Create;
end;

function TUserControl.DoUserLogin: Boolean;
var
   FrmUserLoginForm: TFrmUserLoginForm;
begin
   FrmUserLoginForm := TFrmUserLoginForm.Create(nil);
   try
      Result := (FrmUserLoginForm.ShowModal = mrOk);
   finally
      FrmUserLoginForm.Free;
   end;
end;

procedure TUserControl.SetName(Value: String);
begin
   FName := Value;
end;

procedure TUserControl.SetUsername(Value: String);
begin
   FUsername := Value;
end;

function TUserControl.ValidateUser(Username, Password: String): Boolean;
var
   User: TUser;
begin
   User := TUser.Create;
   if (Username = User.Username) //
      and (Password = User.Password) then
   begin
      FName := User.Name;
      FUsername := User.Username;
      Result := True;
   end
   else
      Result := False;
end;

class function TUserControl.GetInstance: TUserControl;
begin
   If FInstance = nil Then
   begin
      FInstance := uUserControl.TUserControl.Create();
   end;
   Result := FInstance;
end;

end.
