program Example_10;

uses
  Vcl.Forms,
  uMain in 'uMain.pas' {FrmMain},
  uUser in 'uUser.pas',
  uUserControl in 'uUserControl.pas',
  uLogin in 'uLogin.pas' {FrmUserLoginForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmMain, FrmMain);
  Application.CreateForm(TFrmUserLoginForm, FrmUserLoginForm);
  Application.Run;
end.
