unit uUser;

interface

type
   TUser = class
   private
      FName: String;
      FPassword: String;
      FUsername: String;
      procedure SetName(val: String);
      procedure SetPassword(val: String);
      procedure SetUsername(val: String);
   public
      constructor Create;

      property Name: String read FName write SetName;
      property Password: String read FPassword write SetPassword;
      property Username: String read FUsername write SetUsername;
   end;

implementation

constructor TUser.Create;
begin
   FName := 'admin';
   FUsername := 'Administrator';
   FPassword := '1234';
end;

procedure TUser.SetName(val: String);
begin
end;

procedure TUser.SetPassword(val: String);
begin
end;

procedure TUser.SetUsername(val: String);
begin
end;

end.
