unit uLogin;

interface

uses
   Winapi.Windows,
   Winapi.Messages,
   System.SysUtils,
   System.Variants,
   System.Classes,
   Vcl.Graphics,
   Vcl.Controls,
   Vcl.Forms,
   Vcl.Dialogs,
   Vcl.StdCtrls;

type
   TFrmUserLoginForm = class(TForm)
      edtUsername: TEdit;
      edtPassword: TEdit;
      Label1: TLabel;
      Label2: TLabel;
      btnOk: TButton;
      btnCancel: TButton;
      procedure FormClose(Sender: TObject; var Action: TCloseAction);
      procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
      procedure btnOkClick(Sender: TObject);
      procedure btnCancelClick(Sender: TObject);
   private
      procedure CloseForm;
      { Private declarations }
   public
      { Public declarations }
   end;

var
   FrmUserLoginForm: TFrmUserLoginForm;

implementation

uses
   uUser,
   uUserControl;

{$R *.dfm}

procedure TFrmUserLoginForm.btnCancelClick(Sender: TObject);
begin
   CloseForm;
end;

procedure TFrmUserLoginForm.btnOkClick(Sender: TObject);
begin
   CloseForm;
end;

procedure TFrmUserLoginForm.CloseForm;
begin
   Self.Close;
end;

procedure TFrmUserLoginForm.FormClose(Sender: TObject; //
   var Action: TCloseAction);
begin
   Action := TCloseAction.caFree;
end;

procedure TFrmUserLoginForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   if ModalResult = mrOK then
   begin
      if not TUserControl.GetInstance.ValidateUser( //
         edtUsername.Text, edtPassword.Text) then
      begin
         MessageDlg('Invalid user/password!', //
            mtWarning, [mbOK], 0);
         CanClose := False;
      end;
   end;

   if ModalResult = mrCancel then
      Application.Terminate;
end;

end.
