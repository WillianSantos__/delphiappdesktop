object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 374
  ClientWidth = 763
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    AlignWithMargins = True
    Left = 3
    Top = 291
    Width = 757
    Height = 80
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
    ExplicitTop = 44
  end
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 757
    Height = 41
    Align = alTop
    TabOrder = 1
    ExplicitLeft = -3
    ExplicitTop = -3
    ExplicitWidth = 763
    object Label1: TLabel
      Left = 368
      Top = 11
      Width = 34
      Height = 13
      Caption = 'Course'
    end
    object btnOpen: TButton
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 70
      Height = 33
      Align = alLeft
      Caption = 'Open'
      TabOrder = 0
      ExplicitLeft = -8
    end
    object btnInsert: TButton
      AlignWithMargins = True
      Left = 80
      Top = 4
      Width = 70
      Height = 33
      Align = alLeft
      Caption = 'Insert'
      TabOrder = 1
      OnClick = btnInsertClick
      ExplicitTop = 5
    end
    object btnUpdate: TButton
      AlignWithMargins = True
      Left = 156
      Top = 4
      Width = 70
      Height = 33
      Align = alLeft
      Caption = 'Update'
      TabOrder = 2
      OnClick = btnUpdateClick
      ExplicitLeft = 4
    end
    object btnDelete: TButton
      AlignWithMargins = True
      Left = 232
      Top = 4
      Width = 70
      Height = 33
      Align = alLeft
      Caption = 'Delete'
      TabOrder = 3
      OnClick = btnDeleteClick
      ExplicitLeft = 284
      ExplicitTop = 5
    end
    object Edit1: TEdit
      Left = 408
      Top = 8
      Width = 337
      Height = 21
      TabOrder = 4
      Text = 'Name of Course'
    end
  end
  object DBGrid1: TDBGrid
    AlignWithMargins = True
    Left = 3
    Top = 50
    Width = 757
    Height = 235
    Align = alClient
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
end
