program Example_18;

uses
  Vcl.Forms,
  uFrmMain in 'uFrmMain.pas' {Form1},
  Vcl.Themes,
  Vcl.Styles,
  uClassConn in 'Class\uClassConn.pas';

{$R *.res}

begin
   Application.Initialize;
   Application.MainFormOnTaskbar := True;

   {$IFDEF DEBUG}
      Application.Title := 'Example 18 (Debug)';
      TStyleManager.TrySetStyle('Carbon');
   {$ELSE}
      Application.Title := 'Example 18 (Release)';
      TStyleManager.TrySetStyle('Luna');
   {$ENDIF}

   Application.CreateForm(TForm1, Form1);
  Application.Run;

end.
