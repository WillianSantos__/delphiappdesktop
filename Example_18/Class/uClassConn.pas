unit uClassConn;

interface

uses
   System.SysUtils,
   System.Classes,
   FireDAC.Stan.ExprFuncs,
   FireDAC.Phys.SQLiteDef,
   FireDAC.Stan.Intf,
   FireDAC.Phys,
   FireDAC.Phys.SQLite,
   FireDAC.Stan.Option,
   FireDAC.Stan.Error,
   FireDAC.UI.Intf,
   FireDAC.Phys.Intf,
   FireDAC.Stan.Def,
   FireDAC.Stan.Pool,
   FireDAC.Stan.Async,
   FireDAC.VCLUI.Wait,
   Data.DB,
   FireDAC.Comp.Client,
   FireDAC.Stan.Param,
   FireDAC.DatS,
   FireDAC.DApt.Intf,
   FireDAC.DApt,
   FireDAC.Comp.DataSet,
   FireDAC.Comp.UI,
   FireDAC.Stan.StorageJSON,
   FireDAC.Stan.StorageXML,
   FireDAC.Stan.StorageBin;

type
   TConn = class
   private
      FConn: TFDConnection;
      FQry: TFDQuery;

   public
      constructor Create;

      function CourseOpen: TFDQuery;
      function CourseInsert(ACourse: string): Boolean;
      function CourseUpdate(ACourseId, ACourse: string): Boolean;
      function CourseDelete(ACourseId: string): Boolean;

   published
      property Conn: TFDConnection read FConn write FConn;
      property Qry: TFDQuery read FQry write FQry;

   end;

implementation

{ TConn }

constructor TConn.Create;
var
   LFileDB: string;
const
   SQL_TABLE_CREATE = //
      'CREATE TABLE IF NOT EXISTS TB_COURSE (ID_COURSE INTEGER PRIMARY KEY AUTOINCREMENT, DS_COURSE VARCHAR(255), DT_REG_INS DATETIME DEFAULT CURRENT_TIMESTAMP);' + //
      'CREATE TABLE IF NOT EXISTS TB_USER (ID_USER INTEGER PRIMARY KEY AUTOINCREMENT, DS_USER VARCHAR(255), DT_REG_INS DATETIME DEFAULT CURRENT_TIMESTAMP);';
begin
   FConn := TFDConnection.Create(nil);

   FConn.LoginPrompt := False;
   FConn.DriverName := 'SQLite';

   {$IFDEF DEBUG}
   LFileDB := ExtractFilePath(ParamStr(0)) + 'Example_18_Debug.s3db';
   FConn.Params.Values['LockingMode'] := 'lmNormal';
   {$ELSE}
   LFileDB := ExtractFilePath(ParamStr(0)) + 'Example_18_Release.s3db';
   FConn.Params.Values['LockingMode'] := 'lmExclusive';
   {$ENDIF}
   FConn.Params.Values['Database'] := LFileDB;

   FConn.ExecSQL(SQL_TABLE_CREATE);
end;

function TConn.CourseOpen: TFDQuery;
begin
   FQry := TFDQuery.Create(nil);
   FQry.Connection := FConn;
   FQry.Open('SELECT * FROM TB_COURSE');
   Result := FQry;
end;

function TConn.CourseInsert(ACourse: string): Boolean;
begin
   FQry := TFDQuery.Create(nil);
   FQry.Connection := FConn;
   try
      FQry.ExecSQL('INSERT INTO TB_COURSE (DS_COURSE) VALUES (' + QuotedStr(ACourse) + ');');
      Result := True;
   except
      Result := False;
   end;
end;

function TConn.CourseUpdate(ACourseId, ACourse: string): Boolean;
begin
   FQry := TFDQuery.Create(nil);
   FQry.Connection := FConn;
   try
      FQry.ExecSQL('UPDATE TB_COURSE SET DS_COURSE = ' + QuotedStr(ACourse) + ' WHERE ID_COURSE = ' + ACourseId);
      Result := True;
   except
      Result := False;
   end;
end;

function TConn.CourseDelete(ACourseId: string): Boolean;
begin
   FQry := TFDQuery.Create(nil);
   FQry.Connection := FConn;
   try
      FQry.ExecSQL('DELETE FROM TB_COURSE WHERE ID_COURSE = ' + ACourseId);
      Result := True;
   except
      Result := False;
   end;
end;

end.
