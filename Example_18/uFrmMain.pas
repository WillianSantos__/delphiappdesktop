unit uFrmMain;

interface

uses
   Winapi.Windows,
   Winapi.Messages,
   System.SysUtils,
   System.Variants,
   System.Classes,
   Vcl.Graphics,
   Vcl.Controls,
   Vcl.Forms,
   Vcl.Dialogs,
   Vcl.StdCtrls,
   Vcl.ExtCtrls,
   Data.DB,
   Vcl.Grids,
   Vcl.DBGrids,

   FireDAC.Comp.Client,

   uClassConn;

type
   TForm1 = class(TForm)
      btnOpen: TButton;
      Memo1: TMemo;
      Panel1: TPanel;
      btnInsert: TButton;
      btnUpdate: TButton;
      btnDelete: TButton;
      DBGrid1: TDBGrid;
      Label1: TLabel;
      Edit1: TEdit;
      procedure FormCreate(Sender: TObject);
      procedure FormShow(Sender: TObject);
      procedure FormDestroy(Sender: TObject);
      procedure btnInsertClick(Sender: TObject);
      procedure btnUpdateClick(Sender: TObject);
      procedure btnDeleteClick(Sender: TObject);
   private
      ClassConn: TConn;
      FormDataSource: TDataSource;
      procedure FormDataUpdate;
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
   Self.Caption := Application.Title;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
   if not Assigned(ClassConn) then
      ClassConn := TConn.Create;

   if not Assigned(FormDataSource) then
   begin
      FormDataSource := TDataSource.Create(Self);
      FormDataSource.DataSet := ClassConn.CourseOpen;
      DBGrid1.DataSource := FormDataSource;
   end;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
   if Assigned(ClassConn) then
      FreeAndNil(ClassConn);

   if Assigned(FormDataSource) then
      FreeAndNil(FormDataSource);
end;

procedure TForm1.btnInsertClick(Sender: TObject);
begin
   ClassConn.CourseInsert(Edit1.Text);
   FormDataUpdate;
end;

procedure TForm1.btnUpdateClick(Sender: TObject);
begin
   ClassConn.CourseUpdate(FormDataSource.DataSet.Fields[0].Value, Edit1.Text);
   FormDataUpdate;
end;

procedure TForm1.btnDeleteClick(Sender: TObject);
begin
   ClassConn.CourseDelete(FormDataSource.DataSet.Fields[0].Value);
   FormDataUpdate;
end;

procedure TForm1.FormDataUpdate;
begin
   FormDataSource.DataSet.Close;
   FormDataSource.DataSet.Open;
end;

end.
