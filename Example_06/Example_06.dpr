program Example_06;

uses
  Vcl.Forms,
  Vcl.Themes,
  Vcl.Styles,
  uFrmMain in 'uFrmMain.pas' {FrmMain};

{$R *.res}

begin
   Application.Initialize;
   Application.MainFormOnTaskbar := True;
   Application.Title := 'Example 06';
  TStyleManager.TrySetStyle('Luna');
  Application.CreateForm(TFrmMain, FrmMain);
  Application.Run;

end.
