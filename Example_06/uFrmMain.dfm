object FrmMain: TFrmMain
  Left = 0
  Top = 0
  Caption = 'FrmMain'
  ClientHeight = 402
  ClientWidth = 603
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    AlignWithMargins = True
    Left = 3
    Top = 34
    Width = 597
    Height = 365
    Align = alClient
    DataSource = DataSource
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object btnLoad: TButton
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 597
    Height = 25
    Align = alTop
    Caption = 'Load'
    TabOrder = 1
    OnClick = btnLoadClick
  end
  object FDMemTable: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvPersistent, rvSilentMode]
    ResourceOptions.Persistent = True
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 496
    Top = 40
  end
  object DataSource: TDataSource
    DataSet = FDMemTable
    Left = 496
    Top = 88
  end
  object OpenDialog: TOpenDialog
    DefaultExt = '*.fds;*.xml;*.json'
    Filter = 
      'Binary Files ( FireDAC )|*.fds|XML Files ( FireDAC )|*.xml|JSON ' +
      'Files ( FireDAC )|*.json'
    InitialDir = 'C:'
    Options = [ofHideReadOnly, ofEnableSizing, ofForceShowHidden]
    Title = 'FireDAC Files'
    Left = 496
    Top = 136
  end
  object FDStanStorageBinLink: TFDStanStorageBinLink
    Left = 376
    Top = 40
  end
  object FDStanStorageXMLLink: TFDStanStorageXMLLink
    Left = 376
    Top = 88
  end
  object FDStanStorageJSONLink: TFDStanStorageJSONLink
    Left = 376
    Top = 136
  end
end
