unit uFrmMain;

interface

uses
   Winapi.Windows,
   Winapi.Messages,
   System.SysUtils,
   System.Variants,
   System.Classes,
   Vcl.Graphics,
   Vcl.Controls,
   Vcl.Forms,
   Vcl.Dialogs,
   FireDAC.Stan.Intf,
   FireDAC.Stan.Option,
   FireDAC.Stan.Param,
   FireDAC.Stan.Error,
   FireDAC.DatS,
   FireDAC.Phys.Intf,
   FireDAC.DApt.Intf,
   Data.DB,
   FireDAC.Comp.DataSet,
   FireDAC.Comp.Client,
   FireDAC.Stan.StorageBin,
   Vcl.StdCtrls,
   Vcl.Grids,
   Vcl.DBGrids,
   FireDAC.Stan.StorageJSON,
   FireDAC.Stan.StorageXML;

type
   TFrmMain = class(TForm)
      FDMemTable: TFDMemTable;
      DBGrid1: TDBGrid;
      btnLoad: TButton;
      DataSource: TDataSource;
      OpenDialog: TOpenDialog;
      FDStanStorageBinLink: TFDStanStorageBinLink;
      FDStanStorageXMLLink: TFDStanStorageXMLLink;
      FDStanStorageJSONLink: TFDStanStorageJSONLink;
      procedure btnLoadClick(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   FrmMain: TFrmMain;

implementation

{$R *.dfm}

procedure TFrmMain.btnLoadClick(Sender: TObject);
begin
   if OpenDialog.Execute then
   begin
      FDMemTable.LoadFromFile(OpenDialog.FileName, sfAuto);
   end;
end;

end.
