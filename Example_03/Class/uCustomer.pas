unit uCustomer;

interface

type
   TCustomer = class
      Name: string;
      Location: string;
      Terms: string;
      Title: string;
      constructor Create;
   end;

implementation

{ TCustomer }

constructor TCustomer.Create;
begin
   Name := 'The Customer';
   Location := 'New York, NY';
   Terms := 'Net 30';
end;

end.
