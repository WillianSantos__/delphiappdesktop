unit uRetailer;

interface

uses
   uCustomer;

type
   TRetailer = class(TCustomer)
      MultipleLocations: Boolean;
      constructor Create;
   end;

implementation

{ TRetailer }

constructor TRetailer.Create;
begin
   Name := 'The Retailer';
   Location := 'Orlando, MI';
   Terms := 'Cod 42';
   Title := 'More..';
   MultipleLocations := True;
end;

end.
