program Example_03;

uses
  Vcl.Forms,
  uFrmMain in 'uFrmMain.pas' {Form1},
  Vcl.Themes,
  Vcl.Styles,
  uCustomer in 'Class\uCustomer.pas',
  uRetailer in 'Class\uRetailer.pas';

{$R *.res}

begin
   Application.Initialize;
   Application.MainFormOnTaskbar := True;

   {$IFDEF DEBUG}
      Application.Title := 'Example 03 (Debug)';
      TStyleManager.TrySetStyle('Carbon');
   {$ELSE}
      Application.Title := 'Example 03 (Release)';
      TStyleManager.TrySetStyle('Luna');
   {$ENDIF}

   Application.CreateForm(TForm1, Form1);
  Application.Run;

end.
