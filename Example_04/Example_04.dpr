program Example_04;

uses
  Vcl.Forms,
  uFrmPrincipal in 'uFrmPrincipal.pas' {FrmPrincipal},
  Vcl.Themes,
  Vcl.Styles,
  uPessoa in 'Classes\uPessoa.pas',
  uEndereco in 'Classes\uEndereco.pas',
  uContato in 'Classes\uContato.pas',
  uPessoaJuridica in 'Classes\uPessoaJuridica.pas',
  uPessoaFisica in 'Classes\uPessoaFisica.pas';

{$R *.res}

begin
   Application.Initialize;
   Application.MainFormOnTaskbar := True;

   {$IFDEF DEBUG}
   Application.Title := 'Example 04 (Debug)';
   TStyleManager.TrySetStyle('Carbon');
   {$ELSE}
   Application.Title := 'Example 04 (Release)';
   TStyleManager.TrySetStyle('Luna');
   {$ENDIF}
   Application.CreateForm(TFrmPrincipal, FrmPrincipal);
  Application.Run;

end.
