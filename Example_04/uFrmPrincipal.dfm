object FrmPrincipal: TFrmPrincipal
  Left = 0
  Top = 0
  Caption = 'Exemplo 04 (POO)'
  ClientHeight = 211
  ClientWidth = 418
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object btnTestar: TButton
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 412
    Height = 25
    Align = alTop
    Caption = 'Testar'
    TabOrder = 0
    OnClick = btnTestarClick
  end
  object mmTestes: TMemo
    AlignWithMargins = True
    Left = 3
    Top = 34
    Width = 412
    Height = 174
    Align = alClient
    TabOrder = 1
  end
end
