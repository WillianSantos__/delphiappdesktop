unit uEndereco;

interface

type
   TEnderecoTipo = (etRua, etAvenida, etEstrada, etPraca, etRodovia, etViela);

   TEndereco = class
      Tipo: TEnderecoTipo;
      Logradouro: string;
      Numero: string;
      Complemento: string;
      Bairro: string;
      Cidade: string;
      UF: string;
      CEP: string;
      IBGE: Integer;
   end;

implementation

end.
