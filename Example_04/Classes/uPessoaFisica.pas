unit uPessoaFisica;

interface

uses
   uPessoa;

type
   TTipo = (Masculino, Feminino, Outros, Indefinido);

   TPessoaFisica = class(TPessoa)
      CPF: string;
      RG: string;
      CNH: string;
      DataNascimento: TDate;
      Sexo: TTipo;
   end;

implementation

end.
