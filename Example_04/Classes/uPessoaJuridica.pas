unit uPessoaJuridica;

interface

uses
   uPessoa;

type
   TPessoaJuridica = class(TPessoa)
      Fantasia: string;
      CNPJ: string;
      InscricaoEstadual: string;
      InscricaoMunicipal: string;
      DataAbertura: TDate;
      CapitalSocial: Double;
   end;

implementation

end.
