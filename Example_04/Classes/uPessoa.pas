unit uPessoa;

interface

uses
   uEndereco,
   uContato;

type
   TPessoa = class
      NomeRazao: string;
      Endereco: TEndereco;
      Contato: TContato;
      constructor Create;
   end;

implementation

{ TPessoa }

constructor TPessoa.Create;
begin
   Endereco := TEndereco.Create;
   Endereco.IBGE := 0;

   Contato := TContato.Create;
   Contato.Ramal := 'N�o Possui';
end;

end.
