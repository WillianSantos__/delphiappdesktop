unit uFrmPrincipal;

interface

uses
   Winapi.Windows,
   Winapi.Messages,
   System.SysUtils,
   System.Variants,
   System.Classes,
   Vcl.Graphics,
   Vcl.Controls,
   Vcl.Forms,
   Vcl.Dialogs,
   Vcl.StdCtrls,

   // RTTI
   System.RTTI,
   System.Generics.Collections,

   // Classes
   uPessoaJuridica,
   uEndereco;

type
   TFrmPrincipal = class(TForm)
      btnTestar: TButton;
      mmTestes: TMemo;
      procedure FormCreate(Sender: TObject);
      procedure btnTestarClick(Sender: TObject);
   private
      procedure Escrever(ATexto: string);
      { Private declarations }
   public
      { Public declarations }
   end;

var
   FrmPrincipal: TFrmPrincipal;

implementation

{$R *.dfm}

procedure TFrmPrincipal.FormCreate(Sender: TObject);
begin
   Self.Caption := Application.Title;
end;

procedure TFrmPrincipal.Escrever(ATexto: string);
begin
   mmTestes.Lines.Add(ATexto);
end;

procedure TFrmPrincipal.btnTestarClick(Sender: TObject);
var
   PJ: TPessoaJuridica;
begin
   mmTestes.Lines.Clear;

   PJ := TPessoaJuridica.Create;
   try
      PJ.NomeRazao := 'Arcos Dourados Ltda.';
      PJ.Fantasia := 'Mc Donald''s';
      PJ.CNPJ := '01.234.567/0001-98';
      PJ.DataAbertura := StrToDate('1/1/1990');
      PJ.CapitalSocial := 500000.00;
      PJ.InscricaoEstadual := '233.123.645.789';
      PJ.InscricaoMunicipal := '123456/SP';

      {$REGION 'Pessoa Juridica - Endereco'}
      PJ.Endereco.Tipo := TEnderecoTipo.etAvenida;
      PJ.Endereco.Logradouro := 'Paulista';
      PJ.Endereco.Numero := '990';
      PJ.Endereco.Complemento := '2. Andar';
      PJ.Endereco.Bairro := 'Centro';
      PJ.Endereco.Cidade := 'S�o Paulo';
      PJ.Endereco.UF := 'SP';
      PJ.Endereco.CEP := '02314-667';
      PJ.Endereco.IBGE := 356237;
      {$ENDREGION}
      PJ.Contato.Telefone := '+55 (11) 3814-2501';
      PJ.Contato.Email := 'arcosdourados@mcdonalds.com';

      Escrever(PJ.NomeRazao);

   finally
      FreeAndNil(PJ);
   end;
end;

end.
