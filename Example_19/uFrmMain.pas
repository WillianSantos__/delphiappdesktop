unit uFrmMain;

interface

uses
   Winapi.Windows,
   Winapi.Messages,
   System.SysUtils,
   System.Variants,
   System.Classes,
   Vcl.Graphics,
   Vcl.Controls,
   Vcl.Forms,
   Vcl.Dialogs,
   Vcl.StdCtrls,
   IdBaseComponent,
   IdComponent,
   IdTCPConnection,
   IdTCPClient,
   IdHTTP;

type
   TForm1 = class(TForm)
      Button1: TButton;
      Memo1: TMemo;
      IdHTTP1: TIdHTTP;
      Edit1: TEdit;
      procedure FormCreate(Sender: TObject);
      procedure Button1Click(Sender: TObject);
   private
      function JsonViaCEP(ACEP: string): string;
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form1: TForm1;

implementation

uses
   REST.Json,
   ViaCEP;

{$R *.dfm}

function TForm1.JsonViaCEP(ACEP: string): string;
var
   LStringStream: TStringStream;
begin
   LStringStream := TStringStream.Create;
   try
      IdHTTP1.Request.Accept := 'application/json';
      IdHTTP1.Get('http://viacep.com.br/ws/' + ACEP + '/json/', LStringStream);
      Result := AnsiToUtf8(LStringStream.DataString);
   finally
      FreeAndNil(LStringStream);
   end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
   Self.Caption := Application.Title;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
   ViaCEP: TViaCEP;
begin
   ViaCEP := TViaCEP.Create;
   try
      ViaCEP := TJson.JsonToObject<TViaCEP>(JsonViaCEP(Edit1.Text));

      Memo1.Lines.Add(ViaCEP.CEP);
      Memo1.Lines.Add(ViaCEP.Logradouro);
      Memo1.Lines.Add(ViaCEP.Complemento);
      Memo1.Lines.Add(ViaCEP.Bairro);
      Memo1.Lines.Add(ViaCEP.Localidade);
      Memo1.Lines.Add(ViaCEP.UF);
      Memo1.Lines.Add(ViaCEP.Unidade);
      Memo1.Lines.Add(ViaCEP.GIA);
      Memo1.Lines.Add(ViaCEP.IBGE);
   finally
      FreeAndNil(ViaCEP);
   end;
end;

end.
