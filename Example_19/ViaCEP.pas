unit ViaCEP;

interface

uses
   Generics.Collections,
   Rest.Json;

type

   TViaCEP = class

   private
      FBairro: String;
      FCEP: String;
      FComplemento: String;
      FGIA: String;
      FIBGE: String;
      FLocalidade: String;
      FLogradouro: String;
      FUF: String;
      FUnidade: String;

   public
      property Bairro: String read FBairro write FBairro;
      property CEP: String read FCEP write FCEP;
      property Complemento: String read FComplemento write FComplemento;
      property GIA: String read FGIA write FGIA;
      property IBGE: String read FIBGE write FIBGE;
      property Localidade: String read FLocalidade write FLocalidade;
      property Logradouro: String read FLogradouro write FLogradouro;
      property UF: String read FUF write FUF;
      property Unidade: String read FUnidade write FUnidade;

      function ToJsonString: string;
      class function FromJsonString(AJsonString: string): TViaCEP;
   end;

implementation

{ TViaCEP }

function TViaCEP.ToJsonString: string;
begin
   result := TJson.ObjectToJsonString(self);
end;

class function TViaCEP.FromJsonString(AJsonString: string): TViaCEP;
begin
   result := TJson.JsonToObject<TViaCEP>(AJsonString)
end;

end.
