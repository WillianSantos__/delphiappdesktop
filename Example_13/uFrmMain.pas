unit uFrmMain;

interface

uses
   Winapi.Windows,
   Winapi.Messages,
   System.SysUtils,
   System.Variants,
   System.Classes,
   Vcl.Graphics,
   Vcl.Controls,
   Vcl.Forms,
   Vcl.Dialogs,
   Vcl.StdCtrls,
   Vcl.ExtCtrls,
   Vcl.ComCtrls,
   Xml.xmldom,
   Xml.XMLIntf,
   Xml.XMLDoc;

type
   TForm1 = class(TForm)
      Button1: TButton;
      Memo1: TMemo;
      OpenDialog1: TOpenDialog;
      TreeView1: TTreeView;
      XMLDocument1: TXMLDocument;
      procedure FormCreate(Sender: TObject);
      procedure Button1Click(Sender: TObject);
   private
      procedure AdicionaNo(NoXml: IXMLNode; Pai: TTreeNode);
      procedure LerArquivo(Nome: TFileName);
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
   Self.Caption := Application.Title;
end;

procedure TForm1.AdicionaNo(NoXml: IXMLNode; Pai: TTreeNode);
var
   SubNoXml: IXMLNode;
   Filho: TTreeNode;
   i: Integer;
begin
   Filho := TreeView1.Items.AddChild(Pai, NoXml.NodeName);

   for i := 0 to Pred(NoXml.AttributeNodes.Count) do
   begin
      Filho.Text := Filho.Text + ' ' + //
         NoXml.AttributeNodes.Get(i).NodeName + ' = ' + //
         QuotedStr(NoXml.AttributeNodes.Get(i).Text);
   end;

   if NoXml.HasChildNodes then
   begin
      for i := 0 to Pred(NoXml.ChildNodes.Count) do
      begin
         case NoXml.ChildNodes[i].NodeType of
            ntText:
               begin
                  TreeView1.Items.AddChild(Filho, NoXml.Text);
               end;
         else
            SubNoXml := NoXml.ChildNodes[i];
            AdicionaNo(SubNoXml, Filho);
         end;
      end;
   end;
end;

procedure TForm1.LerArquivo(Nome: TFileName);
begin
   Screen.Cursor := crHourGlass;
   try
      XMLDocument1.LoadFromFile(Nome);
      XMLDocument1.Active := True;
      TreeView1.Items.Clear;
      AdicionaNo(XMLDocument1.DocumentElement, nil);
   finally
      Screen.Cursor := crDefault;
   end;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
   if OpenDialog1.Execute(Handle) then
   begin
      LerArquivo(OpenDialog1.FileName);
   end;
end;

end.
