object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 458
  ClientWidth = 769
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 763
    Height = 25
    Align = alTop
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Memo1: TMemo
    AlignWithMargins = True
    Left = 288
    Top = 34
    Width = 478
    Height = 421
    Align = alRight
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
  end
  object TreeView1: TTreeView
    AlignWithMargins = True
    Left = 3
    Top = 34
    Width = 279
    Height = 421
    Align = alClient
    Indent = 19
    TabOrder = 2
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Files XML|*.xml'
    InitialDir = 'C:\'
    Title = 'Files XML'
    Left = 96
    Top = 57
  end
  object XMLDocument1: TXMLDocument
    Left = 96
    Top = 112
  end
end
